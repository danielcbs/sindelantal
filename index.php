
<?php include_once('inc/header.php'); ?>

	<section class="checkout">
		<div class="checkout--wrap wrap form">

			<h1 class="checkout--ttl">Checkout</h1>

			<div class="checkout__user checkout--step step_JS">
				
				<div class="checkout--subttl">Confime sus datos:</div>
				
				<div class="form__row">
					<label class="form__row--label" for="name">Nombre</label>
					<input class="form__row--input" id="name" type="text" value="Empresa XYZ">
				</div>
				<div class="form__row">
					<label class="form__row--label" for="address1">Dirección</label>
					<input class="form__row--input" id="address1" type="text" value="Calle Río Lerma 4, Piso 6">
				</div>
				<div class="form__row">
					<label class="form__row--label" for="address2">Dirección (continuación)</label>
					<input class="form__row--input" id="address2" type="text" value="Col. Cuauhtémoc, 53000, CDMX">
				</div>
				<div class="form__row">
					<label class="form__row--label" for="tel">Teléfono</label>
					<input class="form__row--input" id="tel" type="text" value="044 (55) 3367-8909">
				</div>

				<div class="form__row">
					<button class="checkout--btn next step_next_JS">Continuar <i class="fa fa-arrow-right"></i></button>
				</div>

			</div>
			
			<div class="checkout__cart checkout--step step_JS">

				<div class="checkout--subttl">Resumen de compra:</div>

				<div class="checkout__item">
					<div class="checkout__item--name">ASUS ZenPad Z300M-A2-GR 16GB</div>
					<div class="checkout__item--detail">Adroid Tablet, IEEE 802.11n, Pizarra, Android 6.0, Negro</div>
					<div class="checkout__item--price"> $3,380.95</div>
					<div class="checkout__item--qty">2</div>
				</div>
				<div class="checkout__item">
					<div class="checkout__item--name">Lenovo TAB3 Essential</div>
					<div class="checkout__item--detail"> ZA0R0029US 7" Android Tablet</div>
					<div class="checkout__item--price"> $1,495.31</div>
					<div class="checkout__item--qty">1</div>
				</div>
				<div class="checkout__item">
					<div class="checkout__item--name">Star Wars Figuras de Acción</div>
					<div class="checkout__item--detail">Star Wars Black Series, AT-AT Driver, 6"</div>
					<div class="checkout__item--price"> $449.00</div>
					<div class="checkout__item--qty">1</div>
				</div>
				<div class="form__row">
					<button class="checkout--btn prev step_prev_JS"><i class="fa fa-arrow-left"></i> Regresar</button>
					<button class="checkout--btn next step_next_JS">Continuar <i class="fa fa-arrow-right"></i></button>
				</div>

			</div>
			
			<div class="checkout__payment checkout--step step_JS">

				<div class="checkout--subttl">Forma de pago:</div>

				<div class="form__row method_JS">
					<input type="radio" name="method" id="oxxo" checked>
					<label class="form__row--detail" for="oxxo">
						<i class="fa fa-shopping-cart"></i>OXXO
					</label>
				</div>

				<div class="form__row method_JS">
					<input type="radio" name="method" id="card">
					<label class="form__row--detail" for="card">
						<i class="fa fa-credit-card"></i>Tarjeta de crédito
						<div class="form__row--form method_form_JS">
							<div class="card-js" data-capture-name="true">
								<input class="card-number">
								<input class="name">
								<input class="expiry-month">
								<input class="expiry-year">
								<input class="cvc">
							</div>
						</div>
					</label>
				</div>

				<div class="form__row method_JS">
					<input type="radio" name="method" id="paypal">
					<label class="form__row--detail" for="paypal">
						<i class="fa fa-cc-paypal"></i>PayPal
					</label>
				</div>

				<div class="form__row method_JS">
					<input type="radio" name="method" id="cash">
					<label class="form__row--detail" for="cash">
						<i class="fa fa-money"></i>Efectivo
					</label>
				</div>

				<div class="form__row">
					<button class="checkout--btn prev step_prev_JS"><i class="fa fa-arrow-left"></i> Regresar</button>
					<button class="checkout--btn next step_pay_JS">Pagar <i class="fa fa-arrow-right"></i></button>
				</div>
			</div>

		</div>
	</section>
	
<?php include_once('inc/footer.php'); ?>
