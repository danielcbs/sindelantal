// jshint asi: true
// jshint devel: true
// jshint unused:false

// x@codekit-prepend "scripts/menuToggle.js"


$(document).ready(function() {

	$('.step_JS').first().slideDown('fast')

	$('.step_next_JS').click(function(e) {
		e.preventDefault()
		$(this).closest('.step_JS').slideUp('fast')
		$(this).closest('.step_JS').next('.step_JS').slideDown('fast')
	})

	$('.step_prev_JS').click(function(e) {
		e.preventDefault()
		$(this).closest('.step_JS').slideUp('fast')
		$(this).closest('.step_JS').prev('.step_JS').slideDown('fast')
	})

	$('.method_JS').click(function() {
		if(!$(this).hasClass('active')) {
			$('.method_JS').removeClass('active')
			$(this).addClass('active')
			$('.method_form_JS').slideUp('fast')
			$(this).find('.method_form_JS').slideDown('fast')
		}
	})

})